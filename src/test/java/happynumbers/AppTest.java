package happynumbers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void happyNumbersOneToHundredTest(){

        App app = new App();

        int[] array = new int[]{1, 7, 10, 13, 19, 23, 28, 31, 32, 44, 49, 68, 70, 79, 82, 86, 91, 94, 97, 100};
        for(int number : array){
            boolean actual = app.isHappyNumber(number);
            assertTrue(actual);
        }
    }

    @Test
    public void isHappyNumberTest(){

        App app = new App();
        boolean actual = app.isHappyNumber(19);

        //19 -> 1^2 + 9^2 = 82 -> 8^2 + 2^2 = 68 -> 6^2 + 8^2 = 100 -> 1^2 + 0^2 + 0^2 = 1
        assertTrue(actual);
    }

    @Test
    public void sumSquaresOfDigitsTest(){

        App app = new App();
        int actual = app.sumSquaresOfDigits(19);

        //19 -> 1^2 + 9^2 = 82
        assertTrue(actual==82);
    }

    @Test
    public void tryAgainFalseTest(){

        App app = new App();
        boolean actual = app.tryAgain(3);

        assertFalse(actual);
    }

    @Test
    public void tryAgainTrueTest(){

        App app = new App();
        boolean actual = app.tryAgain(82);

        assertTrue(actual);
    }
}
