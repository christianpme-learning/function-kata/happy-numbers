package happynumbers;

public class App 
{
	public boolean isHappyNumber(final int number) {
		int result = sumSquaresOfDigits(number);
		while (tryAgain(result)) {
			result = sumSquaresOfDigits(result);
		}
		return result == 1;
	}

	public int sumSquaresOfDigits(final int number) {
		final String numerals = Integer.toString(number);
		final char[] charDigits = numerals.toCharArray();
		int sum = 0;
		for (final char charDigit : charDigits) {
			final int digit = Integer.parseInt(String.valueOf(charDigit));
			sum += Math.pow(digit, 2);
		}
		return sum;
	}

	public boolean tryAgain(final int number) {
		final String numerals = Integer.toString(number);
		return numerals.length()>1;
	}
}
